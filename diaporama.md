#### études à l'étranger et mobilité à l'internationale
<!-- .element: data-background-image="https://www.shutterstock.com/image-photo/paper-ships-260nw-649498291.jpg" -->
<p class="fragment.highlight-current-blue.current-fragment">Les mobilités d'un séjour scolaire à l'étranger peuvent recouvrir des formes multiples : individuelles ou collectives, brèves ou longues : échanges, voyages de classes, périodes de scolarité à l'étranger, séquences d'observation, stages ou périodes de formation en milieu professionnel à l'étranger, volontariats de solidarité, service civique, chantiers bénévoles...</p>

- Pour les parents , comment faire ? <!-- .element: class="fragment.highlight-current-blue.current-fragment" -->

- Pour les jeunes quelles infos ?




----

#### Partir à l'étranger : Repères pour les parents

[partir-l-etranger-reperes-pour-les-parents](https://eduscol.education.fr/1103/partir-l-etranger-reperes-pour-les-parents)

[Étudier ou faire un stage en Europe // aides financières](https://www.service-public.fr/particuliers/vosdroits/N13511)



----

#### Partir à l'étranger : Repères pour tous INFOS GENERALES

[EUROGUIDANCE](https://www.euroguidance-france.org/)

[AGITATEURS DE mobilité](https://www.agitateursdemobilite.fr/)

[MOBIDATA](https://cii.edu.ac-lyon.fr/spip/spip.php?rubrique14) 

[IJBOX les programmes européens](https://www.ijbox.fr/dossiers/les-programmes-europeens) 

---

#### Comment ça marche ?

1. Ecole 


----
2. Collège 
Enseignement scolaire
Partir avec Erasmus+ au collège et au lycée
----
3. Lycée

Enseignement scolaire
Partir avec Erasmus+ au collège et au lycée
----
4. Étudiants

----

#### Intéret ?
- Rédaction de présentation très rapide
- Historique des modifications
- Fork pour réutiliser / faire une nouvelle version de présentation

---
<div class="mermaid">
flowchart LR
    2nd[Seconde]
    1G[Première Générale]
    TG[Terminale Générale]
    1T[Première Technologique]
    TT[Terminale Technologique]
    2nd --> 1G
    2nd --> 1T
    1G --> TG
    1T --> TT
</div>

